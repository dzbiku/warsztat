using System.Collections.Generic;
using Warsztat.Models;

namespace Warsztat.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Warsztat.DAL.WsContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Warsztat.DAL.WsContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            var pracownicy = new List<Pracownik>
            {
                new Pracownik
                {
                    Id = 1,
                    DataZatrudnienia = DateTime.Parse("2012-09-01"),
                    DataZwolniania = DateTime.Parse("2012-09-01"),
                    Funkcja = "Blacharz",
                    PrzepracowaneGodziny = 123,
                    Imie = "Janusz",
                    Nazwisko = "A",
                    NrLokalu = 23,
                    Miejscowosc = "Wroclaw",
                    KodPocztowy = "2121",
                    Email = "dsadasd",
                    NrKonta = 12343434,
                    NrTelefonu = 12323,
                    Ulica = "blotna"


                },
                new Pracownik
                {
                    Id = 1,
                    DataZatrudnienia = DateTime.Parse("2012-09-01"),
                    DataZwolniania = DateTime.Parse("2012-09-01"),
                    Funkcja = "Akrobata",
                    PrzepracowaneGodziny = 123,
                    Imie = "Cezary",
                    Nazwisko = "B",
                    NrLokalu = 3,
                    Miejscowosc = "Wroclaw- Bielany",
                    KodPocztowy = "21",
                    Email = "asasasasas@masas.com",
                    NrKonta = 123434,
                    NrTelefonu = 2232323,
                    Ulica = "jakas"

                },
                new Pracownik
                {
                    Id = 1,
                    DataZatrudnienia = DateTime.Parse("2012-09-01"),
                    DataZwolniania = DateTime.Parse("2012-09-01"),
                    Funkcja = "Mechanik",
                    PrzepracowaneGodziny = 123,
                    Imie = "Cezar",
                    Nazwisko = "C",
                    NrLokalu = 3,
                    Miejscowosc = "Wroclaw- PsiePole",
                    KodPocztowy = "21",
                    Email = "asasasasas@masas.com",
                    NrKonta = 1234334,
                    NrTelefonu = 2232323,
                    Ulica = "jakasinna"

                }

            };
            pracownicy.ForEach(s => context.Pracownicy.AddOrUpdate(p => p.Id, s));
            context.SaveChanges();

            var klienci = new List<Klient>
            {
                new Klient
                {
                    Id = 1,
                    Regon = 123456789,
                    Imie = "Cezary",
                    Nazwisko = "A",
                    NrLokalu = 3,
                    Miejscowosc = "Wroclaw- Bielany",
                    KodPocztowy = "21",
                    Email = "asasasasas@masas.com",
                    NrKonta = 123434,
                    NrTelefonu = 2232323,
                    Ulica = "jakas"

                },
                new Klient
                {
                    Id = 2,
                    Regon = 987654321,
                    Imie = "Cezary",
                    Nazwisko = "B",
                    NrLokalu = 3,
                    Miejscowosc = "Wroclaw- Bielany",
                    KodPocztowy = "21",
                    Email = "asasasasas@masas.com",
                    NrKonta = 123434,
                    NrTelefonu = 2232323,
                    Ulica = "jakas"

                },
                new Klient
                {

                    Id = 3,
                    Regon = 123412345,
                    Imie = "Cezary",
                    Nazwisko = "C",
                    NrLokalu = 3,
                    Miejscowosc = "Wroclaw- Bielany",
                    KodPocztowy = "21",
                    Email = "asasasasas@masas.com",
                    NrKonta = 123434,
                    NrTelefonu = 2232323,
                    Ulica = "jakas"

                },

            };
            klienci.ForEach(s => context.Klienci.AddOrUpdate(p => p.Id, s));
            context.SaveChanges();


            var pojedynczeNaprawy = new List<PojedynczaNaprawa>
            {
                new PojedynczaNaprawa
                {
                    PracownikId = pracownicy.Single(s => s.Nazwisko == "C").Id,
                    KlientId = klienci.Single(c => c.Nazwisko == "B").Id,
                    Opis = "cos nie dziala, pomozemy",
                    DataRozpoczecia = DateTime.Parse("2012-09-01"),
                    DataZakonczenia = DateTime.Parse("2012-09-01")
                },
                new PojedynczaNaprawa
                {
                    PracownikId = pracownicy.Single(s => s.Nazwisko == "A").Id,
                    KlientId = klienci.Single(c => c.Nazwisko == "C").Id,
                    Opis = "cos nie dziala, pomozemy albo nie",
                    DataRozpoczecia = DateTime.Parse("2012-09-01"),
                    DataZakonczenia = DateTime.Parse("2012-09-01")
                },
                new PojedynczaNaprawa
                {
                    PracownikId = pracownicy.Single(s => s.Nazwisko == "B").Id,
                    KlientId = klienci.Single(c => c.Nazwisko == "C").Id,
                    Opis = "cos nie dziala, pomozemy albo nie",
                    DataRozpoczecia = DateTime.Parse("2012-09-01"),
                    DataZakonczenia = DateTime.Parse("2012-09-01")
                },


            };

            foreach (var e in pojedynczeNaprawy)
            {
                var enrollmentInDataBase = context.PojedynczaNaprawas.Where(
                    s =>
                        s.Pracownik.Id == e.PracownikId &&
                        s.Klient.Id == e.KlientId).SingleOrDefault();
                if (enrollmentInDataBase == null)
                {
                    context.PojedynczaNaprawas.Add(e);
                }
            }
            context.SaveChanges();


            var faktury = new List<Faktura>
            {
                new Faktura
                {
                    FakturaId = 1,
                    Opis = "Zaplata za naprawy blacharsko-lakiernicze"
                },
                new Faktura
                {
                    FakturaId = 2,
                    Opis = "Zaplata za wymaine silnika"
                },
                new Faktura
                {
                    FakturaId = 3,
                    Opis = "Zaplata za wymiane amortyzatorow"
                },
                new Faktura
                {
                    FakturaId = 4,
                    Opis = "Zaplata za wymaine skrzyni biegow e39 530D"
                },
                new Faktura
                {
                    FakturaId = 5,
                    Opis = "Zaplata za wymiane ogumienia"
                }
            };
            faktury.ForEach(s => context.Faktury.AddOrUpdate(p => p.FakturaId, s));
            context.SaveChanges();

            var potwierdzenia = new List<Potwierdzenie>
            {
                new Potwierdzenie
                {
                    PotwierdzenieId = 1,
                    FakturaId = faktury.Single(s => s.FakturaId == 1).FakturaId,
                    Odebrane = true
                },
                new Potwierdzenie
                {
                    PotwierdzenieId = 2,
                    FakturaId = faktury.Single(s => s.FakturaId == 1).FakturaId,
                    Odebrane = false
                },
                new Potwierdzenie
                {
                    PotwierdzenieId = 3,
                    FakturaId = faktury.Single(s => s.FakturaId == 2).FakturaId,
                    Odebrane = true
                },
                new Potwierdzenie
                {
                    PotwierdzenieId = 4,
                    FakturaId = faktury.Single(s => s.FakturaId == 2).FakturaId,
                    Odebrane = true
                },
                new Potwierdzenie
                {
                    PotwierdzenieId = 5,
                    FakturaId = faktury.Single(s => s.FakturaId == 3).FakturaId,
                    Odebrane = false
                },
                new Potwierdzenie
                {
                    PotwierdzenieId = 6,
                    FakturaId = faktury.Single(s => s.FakturaId == 3).FakturaId,
                    Odebrane = true
                }
            };
            potwierdzenia.ForEach(s => context.Potwierdzenia.AddOrUpdate(p => p.PotwierdzenieId, s));
            context.SaveChanges();
        }
    }
}

