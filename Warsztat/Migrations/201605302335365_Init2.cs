namespace Warsztat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Klient", "Regon", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Klient", "Regon", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
