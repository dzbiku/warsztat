namespace Warsztat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Faktura",
                c => new
                    {
                        FakturaId = c.Int(nullable: false, identity: true),
                        Opis = c.String(),
                    })
                .PrimaryKey(t => t.FakturaId);
            
            CreateTable(
                "dbo.Potwierdzenie",
                c => new
                    {
                        PotwierdzenieId = c.Int(nullable: false, identity: true),
                        Odebrane = c.Boolean(nullable: false),
                        FakturaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PotwierdzenieId)
                .ForeignKey("dbo.Faktura", t => t.FakturaId, cascadeDelete: true)
                .Index(t => t.FakturaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Potwierdzenie", "FakturaId", "dbo.Faktura");
            DropIndex("dbo.Potwierdzenie", new[] { "FakturaId" });
            DropTable("dbo.Potwierdzenie");
            DropTable("dbo.Faktura");
        }
    }
}
