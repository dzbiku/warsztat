namespace Warsztat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Klient",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Regon = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Imie = c.String(),
                        Nazwisko = c.String(),
                        Miejscowosc = c.String(),
                        KodPocztowy = c.String(),
                        Ulica = c.String(),
                        NrLokalu = c.Int(nullable: false),
                        NrTelefonu = c.Int(nullable: false),
                        Email = c.String(),
                        NrKonta = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PojedynczaNaprawa",
                c => new
                    {
                        PojedynczaNaprawaId = c.Int(nullable: false, identity: true),
                        PracownikId = c.Int(nullable: false),
                        KlientId = c.Int(nullable: false),
                        Opis = c.String(),
                        DataRozpoczecia = c.DateTime(nullable: false),
                        DataZakonczenia = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.PojedynczaNaprawaId)
                .ForeignKey("dbo.Klient", t => t.KlientId, cascadeDelete: true)
                .ForeignKey("dbo.Pracownik", t => t.PracownikId, cascadeDelete: true)
                .Index(t => t.PracownikId)
                .Index(t => t.KlientId);
            
            CreateTable(
                "dbo.Pracownik",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Funkcja = c.String(),
                        DataZatrudnienia = c.DateTime(nullable: false),
                        DataZwolniania = c.DateTime(nullable: false),
                        PrzepracowaneGodziny = c.Int(nullable: false),
                        Imie = c.String(),
                        Nazwisko = c.String(),
                        Miejscowosc = c.String(),
                        KodPocztowy = c.String(),
                        Ulica = c.String(),
                        NrLokalu = c.Int(nullable: false),
                        NrTelefonu = c.Int(nullable: false),
                        Email = c.String(),
                        NrKonta = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PojedynczaNaprawa", "PracownikId", "dbo.Pracownik");
            DropForeignKey("dbo.PojedynczaNaprawa", "KlientId", "dbo.Klient");
            DropIndex("dbo.PojedynczaNaprawa", new[] { "KlientId" });
            DropIndex("dbo.PojedynczaNaprawa", new[] { "PracownikId" });
            DropTable("dbo.Pracownik");
            DropTable("dbo.PojedynczaNaprawa");
            DropTable("dbo.Klient");
        }
    }
}
