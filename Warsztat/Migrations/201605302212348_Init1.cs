namespace Warsztat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PojedynczaNaprawa", "DataZakonczenia", c => c.DateTime());
            AlterColumn("dbo.Pracownik", "DataZwolniania", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Pracownik", "DataZwolniania", c => c.DateTime(nullable: false));
            AlterColumn("dbo.PojedynczaNaprawa", "DataZakonczenia", c => c.DateTime(nullable: false));
        }
    }
}
