﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Warsztat.Models;

namespace Warsztat.DAL
{
    public class WsContext: DbContext
    {
        public DbSet<Klient> Klienci { get; set; }
        public DbSet<Pracownik> Pracownicy { get; set; }
        public DbSet<PojedynczaNaprawa> PojedynczaNaprawas { get; set; }
        public DbSet<Potwierdzenie> Potwierdzenia  { get; set; }
        public DbSet<Faktura> Faktury  { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        
    }
}