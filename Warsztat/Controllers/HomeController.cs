﻿using System.Web.Mvc;

namespace Warsztat.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Opis strony z wykorzystaniem ASP.NET";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Strona kontaktowa";

            return View();
        }
    }
}