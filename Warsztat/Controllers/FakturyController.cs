﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Warsztat.DAL;
using Warsztat.Models;

namespace Warsztat.Controllers
{
    public class FakturyController : Controller
    {
        private WsContext db = new WsContext();

        // GET: Faktury
        public ActionResult Index(string sortOrder,string searchString)
        {
            var fakt = from f in db.Faktury
                select f;

            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "Name_desc" : "";

            switch (sortOrder)
            {
                case "Name_desc":
                    fakt = fakt.OrderByDescending(s => s.FakturaId);
                    break;
              
                default:
                    fakt = fakt.OrderBy(s => s.FakturaId);
                    break;
            }

            if (!String.IsNullOrEmpty(searchString))
            {
                fakt = fakt.Where(s => s.Opis.ToUpper().Contains(searchString.ToUpper()));
            }

            return View(fakt.ToList());
        }

        // GET: Faktury/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Faktura faktura = db.Faktury.Find(id);
            if (faktura == null)
            {
                return HttpNotFound();
            }
            return View(faktura);
        }

        // GET: Faktury/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Faktury/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "FakturaId,Opis")] Faktura faktura)
        {
            if (ModelState.IsValid)
            {
                db.Faktury.Add(faktura);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(faktura);
        }

        // GET: Faktury/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Faktura faktura = db.Faktury.Find(id);
            if (faktura == null)
            {
                return HttpNotFound();
            }
            return View(faktura);
        }

        // POST: Faktury/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "FakturaId,Opis")] Faktura faktura)
        {
            if (ModelState.IsValid)
            {
                db.Entry(faktura).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(faktura);
        }

        // GET: Faktury/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Faktura faktura = db.Faktury.Find(id);
            if (faktura == null)
            {
                return HttpNotFound();
            }
            return View(faktura);
        }

        // POST: Faktury/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Faktura faktura = db.Faktury.Find(id);
            db.Faktury.Remove(faktura);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
