﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Warsztat.DAL;
using Warsztat.Models;

namespace Warsztat.Controllers
{
    public class KlientsController : Controller
    {
        private WsContext db = new WsContext();

        // GET: Klients
        public ActionResult Index(string sortOrder, string searchString)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "Name_desc" : "";
            ViewBag.NameSortParm1 = sortOrder == "Name3" ? "Name_desc2" : "Name3";
            ViewBag.NameSortMiej = sortOrder == "Name5" ? "Name4" : "Name5";

            ViewBag.DateSortParm = sortOrder == "Date" ? "Date_desc" : "Date";

            var klienci = from k in db.Klienci
                             select k;


            if (!String.IsNullOrEmpty(searchString))
            {
                klienci = klienci.Where(s => s.Imie.ToUpper().Contains(searchString.ToUpper())
                                       || s.Nazwisko.ToUpper().Contains(searchString.ToUpper()));
            }
            switch (sortOrder)
            {
                case "Name_desc":
                    klienci = klienci.OrderByDescending(s => s.Imie);
                    break;
                case "Name_desc2":
                    klienci = klienci.OrderBy(s => s.Nazwisko);
                    break;
                case "Name3":
                    klienci = klienci.OrderByDescending(s => s.Nazwisko);
                    break;
                case "Name4":
                    klienci = klienci.OrderBy(s => s.Miejscowosc);
                    break;
                case "Name5":
                    klienci = klienci.OrderByDescending(s => s.Miejscowosc);
                    break;
                case "Date":
                    klienci = klienci.OrderBy(s => s.KodPocztowy);
                    break;
                case "Date_desc":
                    klienci = klienci.OrderByDescending(s => s.KodPocztowy);
                    break;
                default:
                    klienci = klienci.OrderBy(s => s.Imie);
                    break;
            }
            return View(klienci.ToList());
        }

        // GET: Klients/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Klient klient = db.Klienci.Find(id);
            if (klient == null)
            {
                return HttpNotFound();
            }
            return View(klient);
        }

        // GET: Klients/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Klients/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Regon,Imie,Nazwisko,Miejscowosc,KodPocztowy,Ulica,NrLokalu,NrTelefonu,Email,NrKonta")] Klient klient)
        {
            if (ModelState.IsValid)
            {
                db.Klienci.Add(klient);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(klient);
        }

        // GET: Klients/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Klient klient = db.Klienci.Find(id);
            if (klient == null)
            {
                return HttpNotFound();
            }
            return View(klient);
        }

        // POST: Klients/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Regon,Imie,Nazwisko,Miejscowosc,KodPocztowy,Ulica,NrLokalu,NrTelefonu,Email,NrKonta")] Klient klient)
        {
            if (ModelState.IsValid)
            {
                db.Entry(klient).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(klient);
        }

        // GET: Klients/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Klient klient = db.Klienci.Find(id);
            if (klient == null)
            {
                return HttpNotFound();
            }
            return View(klient);
        }

        // POST: Klients/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Klient klient = db.Klienci.Find(id);
            db.Klienci.Remove(klient);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
