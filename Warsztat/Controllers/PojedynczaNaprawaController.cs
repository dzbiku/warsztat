﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Warsztat.DAL;
using Warsztat.Models;

namespace Warsztat.Controllers
{
    [SuppressMessage("ReSharper", "ReplaceWithSingleCallToCount")]
    public class PojedynczaNaprawaController : Controller
    {
        private WsContext db = new WsContext();

        // GET: PojedynczaNaprawa
        public ActionResult Index(string sortOrder,  string searchString)
        {
            var pojedynczaNaprawas = from s in db.PojedynczaNaprawas.Include(p => p.Klient).Include(p => p.Pracownik) select s;

            ViewBag.IleZlecen = pojedynczaNaprawas.Where(r => r.Pracownik.Funkcja.Contains("Mechanik")).Count();
            ViewBag.IleBlacharz = pojedynczaNaprawas.Where(r => r.Pracownik.Funkcja.Contains("Blacharz")).Count();
            ViewBag.IleAkrobata = pojedynczaNaprawas.Where(r => r.Pracownik.Funkcja.Contains("Akrobata")).Count();


            if (!String.IsNullOrEmpty(searchString))
            {
                pojedynczaNaprawas = pojedynczaNaprawas.Where(s => s.Klient.Nazwisko.ToUpper().Contains(searchString.ToUpper())
                                       || s.Klient.Imie.ToUpper().Contains(searchString.ToUpper()));
            }


            return View(pojedynczaNaprawas.ToList());
        }

        // GET: PojedynczaNaprawa/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PojedynczaNaprawa pojedynczaNaprawa = db.PojedynczaNaprawas.Find(id);
            if (pojedynczaNaprawa == null)
            {
                return HttpNotFound();
            }
            return View(pojedynczaNaprawa);
        }

        // GET: PojedynczaNaprawa/Create
        public ActionResult Create()
        {
            ViewBag.KlientId = new SelectList(db.Klienci, "Id", "Nazwisko");
            ViewBag.PracownikId = new SelectList(db.Pracownicy, "Id", "Funkcja");
            return View();
        }

        // POST: PojedynczaNaprawa/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PojedynczaNaprawaId,PracownikId,KlientId,Opis,DataRozpoczecia,DataZakonczenia")] PojedynczaNaprawa pojedynczaNaprawa)
        {
            if (ModelState.IsValid)
            {
                db.PojedynczaNaprawas.Add(pojedynczaNaprawa);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.KlientId = new SelectList(db.Klienci, "Id", "Regon", pojedynczaNaprawa.KlientId);
            ViewBag.PracownikId = new SelectList(db.Pracownicy, "Id", "Funkcja", pojedynczaNaprawa.PracownikId);
            return View(pojedynczaNaprawa);
        }

        // GET: PojedynczaNaprawa/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PojedynczaNaprawa pojedynczaNaprawa = db.PojedynczaNaprawas.Find(id);
            if (pojedynczaNaprawa == null)
            {
                return HttpNotFound();
            }
            ViewBag.KlientId = new SelectList(db.Klienci, "Id", "Regon", pojedynczaNaprawa.KlientId);
            ViewBag.PracownikId = new SelectList(db.Pracownicy, "Id", "Funkcja", pojedynczaNaprawa.PracownikId);
            return View(pojedynczaNaprawa);
        }

        // POST: PojedynczaNaprawa/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PojedynczaNaprawaId,PracownikId,KlientId,Opis,DataRozpoczecia,DataZakonczenia")] PojedynczaNaprawa pojedynczaNaprawa)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pojedynczaNaprawa).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KlientId = new SelectList(db.Klienci, "Id", "Regon", pojedynczaNaprawa.KlientId);
            ViewBag.PracownikId = new SelectList(db.Pracownicy, "Id", "Funkcja", pojedynczaNaprawa.PracownikId);
            return View(pojedynczaNaprawa);
        }

        // GET: PojedynczaNaprawa/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PojedynczaNaprawa pojedynczaNaprawa = db.PojedynczaNaprawas.Find(id);
            if (pojedynczaNaprawa == null)
            {
                return HttpNotFound();
            }
            return View(pojedynczaNaprawa);
        }

        // POST: PojedynczaNaprawa/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PojedynczaNaprawa pojedynczaNaprawa = db.PojedynczaNaprawas.Find(id);
            db.PojedynczaNaprawas.Remove(pojedynczaNaprawa);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
