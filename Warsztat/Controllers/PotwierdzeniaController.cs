﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Warsztat.DAL;
using Warsztat.Models;

namespace Warsztat.Controllers
{
    public class PotwierdzeniaController : Controller
    {
        private WsContext db = new WsContext();

        // GET: Potwierdzenia
        public ActionResult Index(string sortOrder, string searchString)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "Name_desc" : "";
            ViewBag.NameSortParm1 = sortOrder == "Name3" ? "Name_desc2" : "Name3";

            var potwierdzenia = db.Potwierdzenia.Include(p => p.Faktura);
            ViewBag.liczba_potwierdzen = potwierdzenia.Where(r => r.FakturaId == 1).Count();

            if (!String.IsNullOrEmpty(searchString))
            {
                potwierdzenia = potwierdzenia.Where(s => s.Odebrane == true);
            }

            switch (sortOrder)
            {
                case "Name_desc":
                    potwierdzenia = potwierdzenia.OrderByDescending(s => s.FakturaId);
                    break;
                case "Name_desc2":
                    potwierdzenia = potwierdzenia.OrderBy(s => s.Odebrane);
                    break;
                case "Name3":
                    potwierdzenia = potwierdzenia.OrderByDescending(s => s.Odebrane);
                    break;
                default:
                    potwierdzenia = potwierdzenia.OrderBy(s => s.FakturaId);
                    break;
            }

            return View(potwierdzenia.ToList());
        }

        // GET: Potwierdzenia/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Potwierdzenie potwierdzenie = db.Potwierdzenia.Find(id);
            if (potwierdzenie == null)
            {
                return HttpNotFound();
            }
            return View(potwierdzenie);
        }

        // GET: Potwierdzenia/Create
        public ActionResult Create()
        {
            ViewBag.FakturaId = new SelectList(db.Faktury, "FakturaId", "Opis");
            return View();
        }

        // POST: Potwierdzenia/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PotwierdzenieId,Odebrane,FakturaId")] Potwierdzenie potwierdzenie)
        {
            if (ModelState.IsValid)
            {
                db.Potwierdzenia.Add(potwierdzenie);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FakturaId = new SelectList(db.Faktury, "FakturaId", "Opis", potwierdzenie.FakturaId);
            return View(potwierdzenie);
        }

        // GET: Potwierdzenia/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Potwierdzenie potwierdzenie = db.Potwierdzenia.Find(id);
            if (potwierdzenie == null)
            {
                return HttpNotFound();
            }
            ViewBag.FakturaId = new SelectList(db.Faktury, "FakturaId", "Opis", potwierdzenie.FakturaId);
            return View(potwierdzenie);
        }

        // POST: Potwierdzenia/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PotwierdzenieId,Odebrane,FakturaId")] Potwierdzenie potwierdzenie)
        {
            if (ModelState.IsValid)
            {
                db.Entry(potwierdzenie).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FakturaId = new SelectList(db.Faktury, "FakturaId", "Opis", potwierdzenie.FakturaId);
            return View(potwierdzenie);
        }

        // GET: Potwierdzenia/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Potwierdzenie potwierdzenie = db.Potwierdzenia.Find(id);
            if (potwierdzenie == null)
            {
                return HttpNotFound();
            }
            return View(potwierdzenie);
        }

        // POST: Potwierdzenia/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Potwierdzenie potwierdzenie = db.Potwierdzenia.Find(id);
            db.Potwierdzenia.Remove(potwierdzenie);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
