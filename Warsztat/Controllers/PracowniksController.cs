﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Warsztat.DAL;
using Warsztat.Models;

namespace Warsztat.Controllers
{
    public class PracowniksController : Controller
    {
        private WsContext db = new WsContext();

        // GET: Pracowniks
        public ActionResult Index(string sortOrder, string searchString)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "Name_desc" : "";
            ViewBag.NameSortParm1 = sortOrder == "Name3" ? "Name_desc2" : "Name3";
            ViewBag.NameSortMiej = sortOrder == "Name5" ? "Name4" : "Name5";

            ViewBag.DateSortParm = sortOrder == "Date" ? "Date_desc" : "Date";

            var pracownicy = from s in db.Pracownicy
                select s;

            if (!String.IsNullOrEmpty(searchString))
            {
                pracownicy = pracownicy.Where(s => s.Imie.ToUpper().Contains(searchString.ToUpper())
                                       || s.Nazwisko.ToUpper().Contains(searchString.ToUpper()));
            }

            switch (sortOrder)
            {
                case "Name_desc":
                    pracownicy = pracownicy.OrderByDescending(s => s.Imie);
                    break;
                case "Name_desc2":
                    pracownicy = pracownicy.OrderBy(s => s.Nazwisko);
                    break;
                case "Name3":
                    pracownicy = pracownicy.OrderByDescending(s => s.Nazwisko);
                    break;
                case "Name4":
                    pracownicy = pracownicy.OrderBy(s => s.Miejscowosc);
                    break;
                case "Name5":
                    pracownicy = pracownicy.OrderByDescending(s => s.Miejscowosc);
                    break;
                case "Date":
                    pracownicy = pracownicy.OrderBy(s => s.PrzepracowaneGodziny);
                    break;
                case "Date_desc":
                    pracownicy = pracownicy.OrderByDescending(s => s.PrzepracowaneGodziny);
                    break;
                default:
                    pracownicy = pracownicy.OrderBy(s => s.Imie);
                    break;
            }

            ViewBag.rates = db.Pracownicy.Average(r => r.PrzepracowaneGodziny);


            return View(pracownicy.ToList());
        }

        // GET: Pracowniks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pracownik pracownik = db.Pracownicy.Find(id);
            if (pracownik == null)
            {
                return HttpNotFound();
            }
            return View(pracownik);
        }

        // GET: Pracowniks/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Pracowniks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Funkcja,DataZatrudnienia,DataZwolniania,PrzepracowaneGodziny,Imie,Nazwisko,Miejscowosc,KodPocztowy,Ulica,NrLokalu,NrTelefonu,Email,NrKonta")] Pracownik pracownik)
        {
            if (ModelState.IsValid)
            {
                db.Pracownicy.Add(pracownik);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(pracownik);
        }

        // GET: Pracowniks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pracownik pracownik = db.Pracownicy.Find(id);
            if (pracownik == null)
            {
                return HttpNotFound();
            }
            return View(pracownik);
        }

        // POST: Pracowniks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Funkcja,DataZatrudnienia,DataZwolniania,PrzepracowaneGodziny,Imie,Nazwisko,Miejscowosc,KodPocztowy,Ulica,NrLokalu,NrTelefonu,Email,NrKonta")] Pracownik pracownik)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pracownik).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pracownik);
        }
/*
        // GET: Pracowniks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pracownik pracownik = db.Pracownicy.Find(id);
            if (pracownik == null)
            {
                return HttpNotFound();
            }
            return View(pracownik);
        }

        // POST: Pracowniks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Pracownik pracownik = db.Pracownicy.Find(id);
            db.Pracownicy.Remove(pracownik);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        */
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
/*
        private static double GetAvg(int id)
        {
            double RatingAverage = db.Pracownicy.Where(r => r.Id ==id).Average(r => r.PrzepracowaneGodziny);

            return RatingAverage;
        }*/
    }
}
