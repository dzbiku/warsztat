﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Warsztat.Models
{
    public class Faktura
    {
        public Faktura()
        {
            this.Potwierdzenie = new HashSet<Potwierdzenie>();
        }

        public int FakturaId { get; set; }
        public string Opis { get; set; }

        public virtual ICollection<Potwierdzenie> Potwierdzenie { get; private set; }
    }

    public class Potwierdzenie
    {
        [Display(Name = "Id")]
        public int PotwierdzenieId { get; set; }

        public bool Odebrane { get; set; }
        [Display(Name = "Faktura Id")]
        public int FakturaId { get; set; }
        public virtual Faktura Faktura { get; set; }
    }
}