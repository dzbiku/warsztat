﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Warsztat.Models
{
    public class PojedynczaNaprawa
    {
        [Display(Name = "Id")]
        public int PojedynczaNaprawaId { get; set; }
        [Display(Name = "Funkcja pracownika")]
        public int PracownikId { get; set; }
        [Display(Name = "Nazwisko klienta")]
        public int KlientId { get; set; }
        public string Opis { get; set; }
        [Display(Name = "Rozpoczecie")]
        public DateTime DataRozpoczecia { get; set; }
        [Display(Name = "Zakonczenie")]
        public DateTime? DataZakonczenia { get; set; }

        public virtual Klient Klient { get; set; }
        public virtual Pracownik Pracownik { get; set; }
    }
}