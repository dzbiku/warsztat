﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Warsztat.Models
{
    public abstract class Osoba
    {
        public int Id { get; set; }
        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        public string Miejscowosc { get; set; }
        [Display(Name = "Kod Pocztowy")]
        public string KodPocztowy { get; set; }
        public string Ulica { get; set; }
        public int NrLokalu { get; set; }
        public int NrTelefonu { get; set; }
        public string Email { get; set; }
        public decimal NrKonta { get; set; }
    }

    public class Klient: Osoba
    {
        public Klient()
        {
            this.PojedynczaNaprawas = new HashSet<PojedynczaNaprawa>();
        }
        public int Id { get; set; }
        public decimal? Regon { get; set; }
        public virtual ICollection<PojedynczaNaprawa> PojedynczaNaprawas { get; private set; } 
    }

    public class Pracownik: Osoba
    {
        public Pracownik()
        {
            this.PojedynczaNaprawas = new HashSet<PojedynczaNaprawa>();
        }
        public int Id { get; set; }
        public string Funkcja { get; set; }
        [Display(Name = "Zatrudniony")]
        public DateTime DataZatrudnienia { get; set; }
        [Display(Name = "Zwolniony")]
        public DateTime? DataZwolniania { get; set; }
        [Display(Name = "Godz.")]
        public int PrzepracowaneGodziny { get; set; }

        public virtual ICollection<PojedynczaNaprawa> PojedynczaNaprawas { get;  private set; }

    }


}